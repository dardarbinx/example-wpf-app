﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WpfApp.Models.Models;
using WpfApp.Repository;

namespace WpfApp.Service
{
    /// <summary>
    /// NOTES
    /// - Ik ga geen Dependency Injection configureren. Deze solution gaat puur om het nut van alle projecten aan te 
    ///   tonen zonder te veel clutter (buiten de overkill aan comments). Een goed project maakt zo goed als altijd
    ///   gebruik van Dependency Injection ipv "using" blocks.
    ///   
    /// Services bevatten alle Business Logic / Logic / Logica / ... (vul zelf term naar keuze in, kan verschillen
    /// van bedrijf tot bedrijf).
    /// Het belangrijkste dat je moet begrijpen aan deze Service is waarom dat deze afzonderlijk staat van 
    /// de repository en de UI. 
    /// 
    /// De UI en Service moeten los staan van elkaar zodat een app altijd klaar is voor UI-wijzigingen. Stel dat het 
    /// bedrijf ineens bovenop de WPF app, ook een web app wil maken. Dan maak je een apart project voor de web API
    /// ( = aanspreekpunt voor front-end van de website), en leg je een reference naar de service en moet niet alles
    /// worden herschreven.
    /// Of bv wanneer je je logica wil testen -> Maak een Test-project aan en leg een referentie naar de Services om 
    /// makkelijk je methodes te testen.
    /// 
    /// Service los van de Repository zodat je bv makkelijk Mock-Repositories kan aanmaken (Een nep-repository dat niet 
    /// echt langs een database gaat en gewoon random data teruggeeft, handig voor tests ed).
    /// Je kan argumenteren dat in een klein project als deze de Service en Repositoy in 1 project kan. Maar als je later
    /// voor een bedrijf werkt mag je ervan uitgaan dat de projecten zo gigantisch zijn dat dit MOET worden opgesplitst,
    /// voor iedereen's mentale welzijn hahah.
    /// </summary>
    public class LibraryService
    {
        // ONDERSTAAND VOORBEELD is hoe het hoort, met Dependency Injection!
        private readonly BooksRepository _bookRepository;
        private readonly AuthorsRepository _authorRepository;

        //public LibraryService(BooksRepository repository)
        //{
        //    _repository = repository;
        //}

        public LibraryService()
        {
            _bookRepository = new BooksRepository();
            _authorRepository = new AuthorsRepository();
        }

        public List<Book> GetBooks()
        {
            return _bookRepository.GetBooks().ToList();
        }

        public Author GetOrCreateAuthor(string firstName, string lastName)
        {
            // First check if the author already exists. Else, create a new one
            var author = _authorRepository.GetAuthor(firstName, lastName);

            // if (s)he does not exist, create one
            if (author == null)
            {
                author = new Author
                {
                    FirstName = firstName,
                    LastName = lastName
                };
            }
            
            return author;
        }

        public Book GetBook(long bookId)
        {
            return _bookRepository.GetBooks().FirstOrDefault(book => book.Id == bookId);
        }

        public void AddBook(Book book)
        {
            _bookRepository.AddBook(book);
        }

        public void ClearAll()
        {
            _bookRepository.Clear();
        }
    }
}
