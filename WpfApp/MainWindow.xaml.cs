﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp.Models.Models;
using WpfApp.Service;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LibraryService _libraryService;
        private List<TextBox> _textBoxes;

        public MainWindow()
        {
            InitializeComponent();

            // Contains all the logic. Notice how this file only contains UI related logic.
            _libraryService = new LibraryService();

            // Put form fields in an array so we can iterate over them later
            _textBoxes = new List<TextBox>() { AuthorFirstNameField, AuthorLastNameField, TitleField, IsbnField };

            InitializeData();
        }

        public void InitializeData()
        {
            // Fetch Data
            var books = _libraryService.GetBooks();
            GridBooks.ItemsSource = books;
        }

        private void AddBook(object sender, RoutedEventArgs e)
        {
            if (!Validate())
            {
                // First check if the form is valid. If not, you can not add a new Book
                return;
            }
            
            // If the form is valid, add the book.
            var book = new Book
            {
                Author = _libraryService.GetOrCreateAuthor(AuthorFirstNameField.Text, AuthorLastNameField.Text),
                Title = TitleField.Text.ToString(),
                Isbn = long.Parse(IsbnField.Text)
            };

            _libraryService.AddBook(book);
            InitializeData();
            ClearFields();
        }

        private void ClearAll(object sender, RoutedEventArgs e)
        {
            _libraryService.ClearAll();
            InitializeData();
        }

        private void ClearFields()
        {
            AuthorFirstNameField.Clear();
            AuthorLastNameField.Clear();
            TitleField.Clear();
            IsbnField.Clear();
            SetBorderDefault(IsbnField);
        }

        private bool Validate()
        {
            if (!CheckIfAllValuesProvided(_textBoxes))
            {
                // If not every textBox is filled in, return, so the user can edit this.
                return false;
            }
            // Check if the filled in Isbn can be parsed to Long
            if (!long.TryParse(IsbnField.Text, out var isbn))
            {
                SetBorderRed(IsbnField);
                return false;
            }
            return true;
        }

        private bool CheckIfAllValuesProvided(List<TextBox> fields)
        {
            // Go through every textBox. If all of them are filled in,
            //  it will never set allFieldsValid to false, so then we know
            //  our form is nicely filled in.
            var allFieldsValid = true;

            foreach (var field in fields)
            {
                if (string.IsNullOrEmpty(field.Text))
                {
                    SetBorderRed(field);
                    allFieldsValid = false;
                }
                else
                {
                    SetBorderDefault(field);
                }
            }
            return allFieldsValid;
        }

        private void SetBorderRed(TextBox field)
        {
            field.BorderBrush = Brushes.Red;
            field.BorderThickness = new Thickness(2);
        }

        private void SetBorderDefault(TextBox field)
        {
            field.BorderBrush = Brushes.Black;
            field.BorderThickness = new Thickness(1);
        }
    }
}
