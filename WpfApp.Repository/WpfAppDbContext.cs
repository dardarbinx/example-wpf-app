﻿
using System.Data.Entity;
using WpfApp.Models.Models;

/// <summary>
/// De Repository zorgt ervoor dat de data uit de database m.b.v. Entity Framework kan worden verwerkt. 
/// Er zijn ook Model Classes die aangeven hoe de tabellen moeten worden opgesteld.
/// Bij het veranderen van deze models maak je (adhv de Package Manager Console) Migrations aan;
/// Deze Migrations zorgen ervoor dat de Db correct wordt gewijzigd zonder conflicten.
/// </summary>
namespace WpfApp.Repository
{
    public class WpfAppDbContext : DbContext
    {
        /// <summary>
        /// "WpfAppDatabase" is de naam van de ConnectionString.
        /// In de App.config van uw startup(!) project vind je deze onder <connectionStrings>. 
        /// Het is belangrijk dat je in de Package Manager Console als Default Project "WpfApp.Repository" hebt staan,
        /// en als Startup Project (vanboven) WpfApp.UI
        /// 
        /// Uw Repository Project MOET een DbContext Class bevatten zoals deze, 
        /// en deze moet ook DbContext van System.Data.Entity inheriten.
        /// 
        /// Na het aanmaken van deze Class kun je in de Package Manager Console het commando
        /// "Enable-Migrations" uitvoeren, die al een eerste Migration aanmaakt.
        /// 
        /// Elke migration wordt eigenlijk vertaald naar SQL dat tabellen aanmaakt, verwijdert, kolommen toevoegt,...
        /// De reden dat er migrations zijn is dat je niet dezelfde SQL code dubbel wil toevoegen (which fucks up your entire DB)
        /// Dus het voert SQL uit, en voegt dan een nieuwe record toe aan de tabel "_Migrations" zodat EntityFramework weet
        /// dat deze code al eerder is uitgevoerd, en de volgende keer kan worden overgeslagen.
        /// 
        /// Als ik voor de eerste keer Enable-Migrations run (die intern ook Add-Migration runt), zal je zien in Migrations/(...)_initial.cs
        /// dat er commando's staan om de database voor de eerste keer op te stellen. Als ik nu een derde class zou aanmaken,
        /// zou ik opnieuw het commando Add-Migration moeten uitvoeren, waarna deze een tweede Migration aanmaakt.
        /// 
        /// De database wordt aangemaakt adhv het commando "update-database". Als jij deze nu uitvoert in de Package
        /// Manager Console, zou je deze database ook moeten bezitten.
        /// (!) In de ConnectionString wordt verwezen naar de server (localdb)\MSSQLLocalDb. Dus als je deze Database wilt zien
        /// in SQL Server Management Studio, moet je connecteren met de server "(localdb)\MSSQLLocalDb"
        /// 
        /// Afhankelijk van de status van mijn database, zal hij beide commando's, of enkel de laatste uitvoeren:
        /// - Als ik mijn database had geupdatet na de eerste Migration, zal hij de tweede keer enkel de laatste Migration uitvoeren.
        /// - Als ik mijn database eerst wis, zal hij beide Migrations terug uitvoeren.
        /// 
        /// WAAROM EEN REPOSITORY PROJECT?
        /// -> Separation of Concerns. De bedoeling is om alle data af te splitsen van alle logica (en UI).
        /// MAAR WAAROM OPSPLITSEN?
        /// -> Stel dat je deze app gebruikt om een bibliotheek-app te maken. Allemaal goed en wel.
        ///    Stel dat je daarna een tweede app wil maken, bv een smartphone app voor de persoon die de boeken leent.
        ///    Als alles dan in 1 project steekt, ben je gejost. Als alles gesplitst is, kun je een apart project aanmaken
        ///    die hetzelfde Repository-Project aanspreekt.
        /// </summary>
        public WpfAppDbContext() : base("WppAppDatabase")
        {
        }

        /// <summary>
        /// Hier geef je aan van welke models je een tabel wil. 
        /// Misschien lijkt het dubbel werk, maar uw DbContext kan niet rieken welke classes een tabel nodig hebben. :D
        /// </summary>
        public IDbSet<Book> Books { get; set; }
        public IDbSet<Author> Authors { get; set; }
    }
}
