﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models.Models;

namespace WpfApp.Repository
{
    /// <summary>
    /// Het doel van de Repository is puur om de data van de DbContext door te geven naar 
    /// whichever project deze data wil opvragen.
    /// 
    /// Waarom is dit opgesplitst? -> Separation of Concerns
    /// Stel dat je een Fake DbContext wil aanmaken om tests uit te voeren, dan gaat dit zonder problemen,
    /// verander hier gewoon de referentie van 1 DbContext naar de andere.
    /// 
    /// Of stel dat je werkt in een complex project dat verschillende databases aanspreekt (en bijgevolg 
    /// ervoor zorgt dat je hier meerdere DbContexten zal hebben), dan is dit een "wrapper" dat ervoor zorgt
    /// dat je niet met de hassle zit om referenties te leggen naar al die verschillende contexten.
    /// </summary>
    public class BooksRepository 
    {
        public IList<Book> GetBooks()
        {
            using(var context = new WpfAppDbContext())
            {
                // .Include() is de LINQ versie van een (gokje) LEFT OUTER JOIN 
                return context.Books.Include(book => book.Author).ToList();
            }
        }

        public void AddBook(Book book)
        {
            using(var context = new WpfAppDbContext())
            {
                context.Books.Add(book);
                context.SaveChanges();
            }
        }

        public void Clear()
        {
            using (var context = new WpfAppDbContext())
            {
                var books = context.Books.ToList();
                foreach (var book in books)
                {
                    context.Books.Remove(book);
                }
                context.SaveChanges();
            }
        }
    }
}
