﻿using System.Linq;
using WpfApp.Models.Models;

namespace WpfApp.Repository
{
    public class AuthorsRepository
    {
        public Author GetAuthor(string firstName, string lastName)
        {
            using (var context = new WpfAppDbContext())
            {
                return context.Authors.FirstOrDefault(author => author.FirstName.Equals(firstName) && author.LastName.Equals(lastName));
            }
        }
    }
}
    