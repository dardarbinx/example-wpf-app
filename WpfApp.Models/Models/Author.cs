﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// WAAROM STAAN DE MODELS APART?
/// Als de Models in de Repository Class staan, zou elk ander project moeten verwijzen naar WpfApp.Repository.
/// Maar dat willen we voorkomen: Enkel het Service project zou mogen verwijzen naar de Repository.
/// Daarom is er een apart Models-project (in bedrijven meestal een Shared project waar alle gedeelde Classes
/// in worden gestoken), waar elk project dat de models nodig heeft een referentie naar "mag" leggen.
/// </summary>
namespace WpfApp.Models.Models
{
    /// <summary>
    /// Model class voor Author. Bij Code First maakt EntityFramework o.b.v. dit model
    /// een tabel Author. 
    /// </summary>
    public class Author
    {
        /// <summary>
        /// Primary key. Heel belangrijk!
        /// </summary>
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        

        /// <summary>
        /// Omdat een Author meerdere Books kan hebben, moet je een List<Book> toevoegen.
        /// </summary>
        public IList<Book> Books { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
