﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// WAAROM STAAN DE MODELS APART?
/// Als de Models in de Repository Class staan, zou elk ander project moeten verwijzen naar WpfApp.Repository.
/// Maar dat willen we voorkomen: Enkel het Service project zou mogen verwijzen naar de Repository.
/// Daarom is er een apart Models-project (in bedrijven meestal een Shared project waar alle gedeelde Classes
/// in worden gestoken), waar elk project dat de models nodig heeft een referentie naar "mag" leggen.
/// </summary>
namespace WpfApp.Models.Models
{
    public class Book
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Model class voor Book. Bij Code First maakt EntityFramework o.b.v. dit model
        /// een tabel Book, met een foreign key "AuthorId" om de link te kunnen leggen met de Author.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Ervan uitgaande dat een Book 1 Author kan hebben
        /// </summary>
        public Author Author { get; set; }

        public long Isbn { get; set; }
    }
}
